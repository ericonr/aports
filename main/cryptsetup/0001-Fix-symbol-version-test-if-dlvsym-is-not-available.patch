From 20b678c9f3a25c5777c8790f21a773f5657c60f0 Mon Sep 17 00:00:00 2001
From: Milan Broz <gmazyland@gmail.com>
Date: Sat, 21 Aug 2021 13:33:13 +0200
Subject: [PATCH] Fix symbol version test if dlvsym() is not available.

If we have no dlvsym(), just run dlsym() test.
---
 configure.ac             |  5 +++
 tests/all-symbols-test.c | 71 ++++++++++++++++++++++++++--------------
 2 files changed, 52 insertions(+), 24 deletions(-)

diff --git a/configure.ac b/configure.ac
index 61fa8b63..48098b41 100644
--- a/configure.ac
+++ b/configure.ac
@@ -101,6 +101,11 @@ AC_CHECK_LIB(uuid, uuid_clear, ,[AC_MSG_ERROR([You need the uuid library.])])
 AC_SUBST(UUID_LIBS, $LIBS)
 LIBS=$saved_LIBS
 
+saved_LIBS=$LIBS
+AC_SEARCH_LIBS([dlsym],[dl])
+AC_CHECK_FUNCS([dlvsym])
+LIBS=$saved_LIBS
+
 AC_SEARCH_LIBS([clock_gettime],[rt posix4])
 AC_CHECK_FUNCS([posix_memalign clock_gettime posix_fallocate explicit_bzero])
 
diff --git a/tests/all-symbols-test.c b/tests/all-symbols-test.c
index d1bafbdd..adc56162 100644
--- a/tests/all-symbols-test.c
+++ b/tests/all-symbols-test.c
@@ -68,34 +68,57 @@ static void test_logf(int level, const char *format, ...)
 #define log_std(x...) test_logf(LOG_NORMAL, x)
 #define log_err(x...) test_logf(LOG_ERROR, x)
 
-static int check_all_symbols(void *h)
+static int check_dlvsym(void *h, const char *symbol, const char *version)
+{
+#ifdef HAVE_DLVSYM
+	void *sym;
+	char *err;
+
+	log_dbg("Checking %s@%s...", symbol, version);
+	sym = dlvsym(h, symbol, version);
+	UNUSED(sym);
+	err = dlerror();
+
+	if (err) {
+		log_err("%s.", err);
+		return 1;
+	}
+
+	log_dbg("OK\n");
+#endif
+	return 0;
+}
+
+static int check_dlsym(void *h, const char *symbol)
 {
 	void *sym;
 	char *err;
+
+	log_dbg("Checking %s...", symbol);
+	sym = dlsym(h, symbol);
+	UNUSED(sym);
+	err = dlerror();
+
+	if (err) {
+		log_err("%s", err);
+		return 1;
+	}
+
+	log_dbg("OK\n");
+	return 0;
+}
+
+static int check_all_symbols(void *h)
+{
 	unsigned scount = 0;
 
-#define CHECK_SYMBOL(SYM, VER)								\
-do {											\
-	log_dbg("Checking " #SYM "@" #VER "...");					\
-	sym = dlvsym(h, #SYM, #VER);							\
-	UNUSED(sym);									\
-	err = dlerror();								\
-											\
-	if (err) {									\
-		log_err("%s.", err);							\
-		return 1;								\
-	}										\
-											\
-	log_dbg("OK\nChecking " #SYM "...");						\
-	sym = dlsym(h, #SYM);								\
-	UNUSED(sym);									\
-	err = dlerror();								\
-	if (err) {									\
-		log_err("%s", err);							\
-		return 1;								\
-	}										\
-	log_dbg("OK\n");								\
-	scount++;									\
+#define CHECK_SYMBOL(SYM, VER)			\
+do {						\
+	if (check_dlvsym(h, #SYM, #VER))	\
+		return 1;			\
+	if (check_dlsym(h, #SYM))		\
+		return 1;			\
+	scount++;				\
 } while (0);
 
 #include "test-symbols-list.h"
@@ -106,7 +129,7 @@ do {											\
 		return 1;
 	}
 
-	log_std("Performed %u symbol checks in total\n.", scount);
+	log_std("Performed %u symbol checks in total.\n", scount);
 
 	return 0;
 }
