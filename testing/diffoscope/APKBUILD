# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=diffoscope
pkgver=183
pkgrel=0
pkgdesc="In-depth comparison of files, archives, and directories"
url="https://diffoscope.org/"
arch="noarch !mips !mips64" # py3-libarchive-c
license="GPL-3.0-or-later"
makedepends="python3-dev py3-setuptools py3-docutils"
depends="py3-magic py3-libarchive-c"
checkdepends="py3-pytest gzip bzip2 unzip libarchive-tools cdrkit
	openssh-client-default"
source="https://salsa.debian.org/reproducible-builds/diffoscope/-/archive/$pkgver/diffoscope-$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH=".:$PYTHONPATH" PYTHONDONTWRITEBYTECODE=1 py.test \
		-k 'not test_text_proper_indentation'
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
4892f8e46fb2fb5776d2f65d7b3018246b3d780b847ab57f0c68d9f5f1a794050f5b431cb03e5a0db1c24f6421aa872034e8f99b8862fc6fc231ccfd5a198367  diffoscope-183.tar.gz
"
