# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=miniserve
pkgver=0.16.0
pkgrel=0
pkgdesc="Quickly serve files via HTTP"
url="https://github.com/svenstaro/miniserve"
license="MIT"
arch="all !s390x !mips64 !riscv64" # limited by rust/cargo
arch="$arch !ppc64le" # FTBFS
makedepends="cargo"
subpackages="$pkgname-bash-completion $pkgname-fish-completion $pkgname-zsh-completion"
source="https://github.com/svenstaro/miniserve/archive/v$pkgver/miniserve-$pkgver.tar.gz"

build() {
	cargo build --release --locked

	./target/release/miniserve --print-completions bash > $pkgname.bash
	./target/release/miniserve --print-completions fish > $pkgname.fish
	./target/release/miniserve --print-completions zsh > $pkgname.zsh
}

check() {
	cargo test --release --locked
}

package() {
	install -Dm755 target/release/miniserve "$pkgdir"/usr/bin/miniserve

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
e1bd7b63a46571e877ee2fc850d71b85a206a205ce6dab4343de43905e5b3ce12855924ec24b6ea37f5f783e2ca8ef49db25f3f7e7e7b30c13303b670355bf35  miniserve-0.16.0.tar.gz
"
